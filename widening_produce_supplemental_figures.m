function [fig_handle2,C,H] = widening_produce_supplemental_figures(data,opts)

fig_handle2 = figure;

yr = opts.yr_1:opts.yr_end;

subs = find(abs(data.lat)<=50);

[C,H] = contourf(yr,double(data.lat(subs)),squeeze(nanmean(data.annual(:,subs,:),1)));
shading flat
colorbar
title('Zonal-Annual Mean Brightness Temperature (K)')
