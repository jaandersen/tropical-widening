function data=widening_extract_data(opts)




foo = getdata(opts,1);

[nx,ny,nt] = size(foo);

nt_tot = 0;

for j = 1:numel(opts.files)
    nt_tot = nt_tot+numel(ncread(opts.files{j},opts.time_name));
end



datum = zeros(nx,ny,nt_tot);
datum(:,:,1:nt) = foo;
last_position = nt;

if numel(opts.files)>1
    for j = 2:numel(opts.files)
        foo = getdata(opts,j);
        [nx,ny,nt] = size(foo);
        last_position2 = last_position + nt;
        datum(:,:,last_position+1:last_position2) = foo;
        last_position =  last_position2;
    end   
end

data.raw_data = datum;



data.lat = ncread(opts.files{1},opts.latname);
data.lon = ncread(opts.files{1},opts.lonname);

subs = find(abs(data.lat)<=opts.latrange);
data.lat = data.lat(subs);






function foo = getdata(opts,j)

filename = opts.files{j};
disp(['reading ',filename])
foo = ncread(filename,opts.variable);

lon = ncread(filename,opts.lonname);
lat = ncread(filename,opts.latname);
subs = find(abs(lat)<=opts.latrange);

if opts.ND == 4
    foo = foo(:,subs,:,:);
elseif opts.ND ==3
    foo = foo(:,subs,:);
end

if opts.ND==4
    [nx,ny,nz,nt] = size(foo);
    if strcmp(opts.leveltype,'hybrid')
        
        %{
        p0 = ncread(filename,'p0');
        a = ncread(filename,'a');
        b_s = ncread(filename,'b');
        ps = ncread(filename,'ps');
        for j = 1:size(ps,1)
            for k = 1:size(ps,2)
                for l = 1:size(ps,3)
                    p(j,k,:,l) = a*p0 + b_s*ps(j,k,l);
                end
            end
        end
        levels = squeeze(mean(mean(mean(p,4),2),1));  

        %}
        
        if ~strcmp(opts.p0,'none')
            p0 = ncread(filename,opts.p0);
        else 
            p0 = 1;
        end
            ap = ncread(filename,opts.a);
        ap_bnds = ncread(filename,opts.a_bnds);
        ap_bnds = [ap_bnds(1,:),ap_bnds(2,end)];

        ps = ncread(filename,opts.ps);
        ps = ps(:,subs,:);
        
        
        b = ncread(filename,opts.b);
        b_bnds = ncread(filename,opts.b_bnds);
        b_bnds = [b_bnds(1,:),b_bnds(2,end)];


   
        
        lev = ncread(filename,opts.lev);
        lev_bnds = ncread(filename,opts.lev_bnds);
        lev_bnds = [lev_bnds(1,:),lev_bnds(2,end)];


        pp_c = zeros(size(foo));
        pp_i = zeros(nx,ny,nz+1,nt);

        AP = permute(repmat(ap,[1,nx,ny,nt]),[2,3,1,4]);
        B = permute(repmat(b,[1,nx,ny,nt]),[2,3,1,4]);
        PS = permute(repmat(ps,[1,1,1,numel(ap)]),[1,2,4,3]);
        pp_c = (p0*AP+B.*PS)*opts.pressure_scale;

  
        AP_b = permute(repmat(ap_bnds',[1,nx,ny,nt]),[2,3,1,4]);
        B_b = permute(repmat(b_bnds',[1,nx,ny,nt]),[2,3,1,4]);
        PS_b = permute(repmat(ps,[1,1,1,numel(ap_bnds)]),[1,2,4,3]);
        pp_i = (p0*AP_b+B_b.*PS_b)*opts.pressure_scale;
 
        
    else
        
        levels = ncread(filename,opts.levelname)*opts.pressure_scale;
        levelsi = [101325;(levels(2:end) + levels(1:end-1))/2;0];   
        pp_c = repmat(levels,[1,nx,ny,nt]);
        pp_c = permute(pp_c,[2,3,1,4]);
        pp_i = repmat(levelsi,[1,nx,ny,nt]);
        pp_i = permute(pp_i,[2,3,1,4]);
    end
     

    
    foo = convertdata(opts,foo,j,pp_c);
    
    
   foo = calc_column_weighted(foo,double(pp_i),double(pp_c),opts.weight_vector,opts.pressures);%*opts.scale_factor;
end


function foo = convertdata(opts,foo,j,p_c)

if opts.variable =='hus'

    command = ['ls ',opts.ta_name_root,'*.nc > nc_file_list.txt'];
    unix(command);
    fid = fopen('nc_file_list.txt');
    files=textscan(fid,'%s');
    opts.ta_files = files{1};
    fclose(fid);
    
    ta_filename = opts.ta_files{j};
    disp(['reading ',ta_filename])
    
    
    lat = ncread(ta_filename,opts.latname);
    subs = find(abs(lat)<=opts.latrange);
    
    ta = ncread(ta_filename,opts.ta_variable);
    ta = ta(:,subs,:,:);
    qsat = zeros(size(foo));

    thermo_constants

    qsat(:)=qs(p_c(:),ta(:));

    qsat(p_c<12000) = 2.5e-5;

    foo = 100*foo./qsat;
end
    
    
