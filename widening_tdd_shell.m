%Widening_tdd_shell
%
% Run and test the Tropical Widening analysis scripts

%

dotest =0;

experiment ='IPSL rh';

opts = widening_options(experiment);

data = widening_extract_data(opts);

if dotest
   [data,opts] = calculate_simple_test_data(data,opts) 
end

        

data = widening_calc_averages(data,opts);

data = widening_calc_trends(data,opts);

fig_handle = widening_produce_figure(data,opts);

[fig_handle2,C,H] = widening_produce_supplemental_figures(data,opts);


%disp('Test. Should be HIRS data structure')
data
%}
%{
experiment = 'HIRS ch12 BT sample';

opts = widening_options(experiment);

data = widening_extract_data(opts);


disp('Test. Should be HIRS ch12 BT data structure')
data
%}