function data = widening_calc_averages(data,opts)


for yr = opts.yr_1:opts.yr_end
    bt_foo = data.raw_data(:,:,(yr-opts.yr_1)*opts.records_per_year+1:(yr-opts.yr_1+1)*opts.records_per_year);
    data.monthly(:,:,(yr-opts.yr_1)*opts.records_per_year+1:(yr-opts.yr_1+1)*opts.records_per_year) = bt_foo;
    data.annual(:,:,yr-opts.yr_1+1) = nanmean(bt_foo,3);
end


