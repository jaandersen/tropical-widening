function [data,opts] = calculate_simple_test_data(data,opts) 

for j = 1:numel(data.lon)
        for k = 1:numel(data.lat)
            for l = 1:size(data.raw_data,3)
                data.raw_data(j,k,l) =randn(1)/10+ sin(data.lon(j)*pi/180) + cos(data.lat(k)*pi/180)*(l-198)/12;
            end
        end
end
opts.ltm_range = [-1:0.1:1];
opts.trend_range =10*[.5:0.01:1.1];