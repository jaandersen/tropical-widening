function opts=widening_options(exper)


opts.stat_threshold = 0.8;
opts.ND = 4;
opts.latname = 'lat';
opts.lonname = 'lon';
opts.latrange = 60
opts.pressure_scale = 1;
opts.ref_lev = [200,250,300,350,400];
opts.pressures = [0.01;100;10000;15000; 20000;25000;30000;40000;50000;70000;120000];
opts.weight_vector = zeros(numel(opts.pressures),numel(opts.ref_lev));
opts.weight_vector(:,3) = [0;0;0.01;0.07;0.15;0.20;0.27;0.22; 0.07;0.01;0];%p6.7 = 300
opts.weight_vector(:,1) = [0;0;0.12;.26;.29;.2;.11;0;0;0;0]; %p6.7 = 200
opts.weight_vector(:,2) = [0;0;0.04;.14;.22;.24;.23;0.11;0.02;0;0]; %p6.7 = 250
opts.weight_vector(:,4) = [0;0;0.00;.04;.09;.14;.25;0.29;0.16;0.03;0]; %p6.7 = 350
opts.weight_vector(:,5) = [0;0;0.00;.02;.05;.10;.20;0.29;0.25;0.09;0]; %p6.7 = 400
%opts.weight_vector = ones(size(opts.pressures));
%opts.weight_vector(end-4:end) = 0;
opts.ltm_range = [238:2:258];
opts.trend_range = [-8.5:0.3:8.5];

%opts.weight_vector = [0;0;1;1;0;0];
%opts.pressures = [0.01;19900;20000;50000;50100;120000];
opts.levelname = 'plev'
opts.stat_threshold = 0;
opts.time_name = 'time';
opts.records_per_year = 12;

opts.p0 = 'p0';
opts.a = 'a';
opts.a_bnds = 'a_bnds';
opts.ps = 'ps';
opts.b = 'b';
opts.b_bnds = 'b_bnds';
opts.lev = 'lev'
opts.lev_bnds = 'lev_bnds'
switch exper
    case 'HIRS ch12 BT'
        opts.name_root = '~/data/HIRS/HIRS-CH12_MONGRD_v02r05_';
        opts.yr_1 = 1979;
        opts.yr_end = 2011;    
        opts.variable = 'bt'
        opts.ND = 3;
        opts.ltm_range = [238:2:258];
        opts.trend_range = [-1.5:0.3:1.5];
    case 'bccr_bcm2_0 hur'
        opts.name_root = '~/data/CMIP3/hur/pcmdi.ipcc4.bccr_bcm2_0.20c3m.run1.monthly.hur';
        opts.leveltype = 'pressure';
        opts.yr_1 = 1850;
        opts.yr_end = 1999;
        opts.yr_1b = 1978;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hur'; 
        opts.toI = 20000;
        opts.boI = 50000;
        opts.scale_factor = 9.8/abs(opts.boI-opts.toI);
        opts.ltm_range = 0.01*[-50:1:50];
        opts.trend_range = 100*[00:.01:1];   
        
        
       case 'cccma_cgcm3_1 hur'
            opts.name_root = '~/data/CMIP3/hur/pcmdi.ipcc4.cccma_cgcm3_1.20c3m.run1.monthly.hur';
            opts.leveltype = 'hybrid';
            opts.yr_1 = 1850;
            opts.yr_end = 2000;
            opts.yr_1b = 1978;
            opts.yr_endb = 1999;
            opts.stat_thresh = 0.95;
            opts.variable = 'hur'; 
            opts.toI = 20000;
            opts.boI = 50000;
            opts.scale_factor = 9.8/abs(opts.boI-opts.toI);
            opts.ltm_range = [0:1:100];
            opts.trend_range = [-1.5:.1:1.5];  
        
        case 'ERAi rh'

            opts.name_root = '~/data/ERAi/rh/ERAi.rh.';
            opts.variable = 'r';
            opts.range_1 = 3*[-1.5:0.3:1.5];
            opts.range_2 = [10:1:80];
            opts.toI = 200; %pressure at top of integral
            opts.boI = 550; %pressure at bottom of integral
            opts.yr_1=1979;
            opts.yr_end = 2011;
            opts.scale_factor = 9.8/abs(opts.boI-opts.toI);
            opts.yr_1b = 1984;
            opts.yr_endb = 2011;   
            opts.ltm_range = [0:1:70];
            opts.trend_range = [-4.5:.1:4.5];  
            opts.latname = 'latitude';
            opts.lonname = 'longitude';
            opts.leveltype = 'pressure';
            opts.levelname = 'levelist'
            opts.pressure_scale = 100;
            
            
    case 'IPSL BT'
        opts.name_root = '~/data/IPSL/bt.meteosat5.tropical.nadir.19';
             opts.yr_1 = 1979;
        opts.yr_end = 1988;
        opts.variable = 'BT'; 
        opts.ltm_range = [230:1:252];
        opts.trend_range = [-1.5:.1:1.5];  
        opts.latname = 'latitude';
        opts.lonname = 'longitude';
        opts.ND = 3;
        opts.records_per_year = 1460;
        
        
    case 'IPSL rh'
        opts.name_root = '~/data/IPSL/hus_6hrLev_IPSL-CM5A-LR_amip_r1i1p1_';
        opts.ta_name_root = '~/data/IPSL/ta_6hrLev_IPSL-CM5A-LR_amip_r1i1p1_';
        opts.leveltype = 'hybrid';
        opts.levelname = 'lev';
        opts.yr_1 = 1979;
        opts.yr_end = 1988;
        opts.yr_1b = 1980;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hus'; 
        opts.ta_variable = 'ta'; 
        opts.latname = 'lat';
        opts.lonname = 'lon';
        opts.ND = 4;
        opts.records_per_year = 1460;
        opts.p0 = 'none';
        opts.a = 'ap';
        opts.a_bnds = 'ap_bnds';
        opts.ps = 'ps';
        opts.b = 'b';
        opts.b_bnds = 'b_bnds';
        opts.lev = 'lev'
        opts.lev_bnds = 'lev_bnds'
        opts.ltm_range = [0:1:100];

    case 'BCC rh'
        opts.name_root = '~/data/hur_day_bcc-csm1-1_historical_r1i1p1_';
        opts.ta_name_root = '~/data/ta_day_bcc-csm1-1_historical_r1i1p1_';
        opts.leveltype = 'pressure';
        opts.levelname = 'plev';
        opts.yr_end = 1965;
        opts.yr_1 = 1950;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hur'; 
        opts.ta_variable = 'ta'; 
        opts.latname = 'lat';
        opts.lonname = 'lon';
        opts.ND = 4;
        opts.records_per_year = 365;
        opts.p0 = 'none';
        opts.a = 'ap';
        opts.a_bnds = 'ap_bnds';
        opts.ps = 'ps';
        opts.b = 'b';
        opts.b_bnds = 'b_bnds';
        opts.lev = 'lev'
        opts.lev_bnds = 'lev_bnds'
        opts.ltm_range = [0:1:100]; 
        
         case 'BCC sh'
        opts.name_root = '~/data/hus_day_bcc-csm1-1_historical_r1i1p1_';
        opts.ta_name_root = '~/data/ta_day_bcc-csm1-1_historical_r1i1p1_';
        opts.leveltype = 'pressure';
        opts.levelname = 'plev';
        opts.yr_end = 1965;
        opts.yr_1 = 1950;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hus'; 
        opts.ta_variable = 'ta'; 
        opts.latname = 'lat';
        opts.lonname = 'lon';
        opts.ND = 4;
        opts.records_per_year = 365;
        opts.p0 = 'none';
        opts.a = 'ap';
        opts.a_bnds = 'ap_bnds';
        opts.ps = 'ps';
        opts.b = 'b';
        opts.b_bnds = 'b_bnds';
        opts.lev = 'lev'
        opts.lev_bnds = 'lev_bnds'
        opts.ltm_range = [0:1:100]; 
     
    
    case 'MRI rh'
        opts.name_root = '~/data/hur_day_MRI-CGCM3_historical_r1i1p1_';
        opts.ta_name_root = '~/data/ta_day_MRI-CGCM3_historical_r1i1p1_';
        opts.leveltype = 'pressure';
        opts.levelname = 'plev';
        opts.yr_end = 2005;
        opts.yr_1 = 2003;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hur'; 
        opts.ta_variable = 'ta'; 
        opts.latname = 'lat';
        opts.lonname = 'lon';
        opts.ND = 4;
        opts.records_per_year = 365;
        opts.p0 = 'none';
        opts.a = 'ap';
        opts.a_bnds = 'ap_bnds';
        opts.ps = 'ps';
        opts.b = 'b';
        opts.b_bnds = 'b_bnds';
        opts.lev = 'lev'
        opts.lev_bnds = 'lev_bnds'
        opts.ltm_range = [0:1:100];   
        
            case 'MRI sh'
        opts.name_root = '~/data/hus_day_MRI-CGCM3_historical_r1i1p1_';
        opts.ta_name_root = '~/data/ta_day_MRI-CGCM3_historical_r1i1p1_';
        opts.leveltype = 'pressure';
        opts.levelname = 'plev';
        opts.yr_end = 2005;
        opts.yr_1 = 2003;
        opts.yr_endb = 1999;
        opts.stat_thresh = 0.95;
        opts.variable = 'hus'; 
        opts.ta_variable = 'ta'; 
        opts.latname = 'lat';
        opts.lonname = 'lon';
        opts.ND = 4;
        opts.records_per_year = 365;
        opts.p0 = 'none';
        opts.a = 'ap';
        opts.a_bnds = 'ap_bnds';
        opts.ps = 'ps';
        opts.b = 'b';
        opts.b_bnds = 'b_bnds';
        opts.lev = 'lev'
        opts.lev_bnds = 'lev_bnds'
        opts.ltm_range = [0:1:100];   
        
        
        
end







command = ['ls ',opts.name_root,'*.nc > nc_file_list.txt'];
unix(command);
fid = fopen('nc_file_list.txt');
files=textscan(fid,'%s');
opts.files = files{1};
fclose(fid);
