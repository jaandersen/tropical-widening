function data = widening_calc_trends(data,opts)


bt_mean = nanmean(data.annual,3);
for j = 1:size(data.annual,3)
    bt_annual_anomalies(:,:,j) = data.annual(:,:,j) - bt_mean;
end



yr = 1:size(data.annual,3)';

x = [ones(size(yr))', yr'];

for j = 1:size(data.annual,1)
    for k = 1:size(data.annual,2)
        
        
        [data.b(j,k,:),data.bint(j,k,:,:)] = regress(squeeze(bt_annual_anomalies(j,k,:)),x,1-opts.stat_threshold);

    end
end
filename = opts.files{1};

[data.subs_signifx,data.subs_signify] = find(data.bint(:,:,2,1).*data.bint(:,:,2,2)<=0);
data.ltm = bt_mean;


