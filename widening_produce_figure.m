function fig_handle = widening_produce_figure(data,opts)

for j = 1:numel(data.subs_signifx)
    data.b(data.subs_signifx(j),data.subs_signify(j),2) = NaN;
end


fig_handle = figure;


subplot(2,1,1)
prepmap(60);
contourfm(double(data.lat),double(data.lon),data.ltm',opts.ltm_range,'linestyle','none')
caxis([min(opts.ltm_range),max(opts.ltm_range)])
contourcbar
title(['a) Long Term Mean ',num2str(opts.yr_1),'--',num2str(opts.yr_end),'(K)'])



subplot(2,1,2)
foo = prepmap(60);
contourfm(double(data.lat),double(data.lon),10*squeeze(data.b(:,:,2))',opts.trend_range,'linestyle','none')
caxis([min(opts.trend_range),max(opts.trend_range)])
contourcbar
title(['b) Annual Trend ',num2str(opts.yr_1),'--',num2str(opts.yr_end),' (K/decade)'])





