function column = calc_column_weighted(qq,p_i,p_c,weight_vector,pressures);



weights = zeros(size(qq));
weights(:) = interp1(pressures,weight_vector,(p_c(:)),'linear','extrap');


g=9.80616;

p_top = min(p_i(:));
p_bottom = max(p_i(:));

if numel(size(p_i))==4
    pmean = squeeze(mean(mean(mean(p_i,1),2),4));
else
    pmean = p_i;
end




foo = find(pmean>=p_top & pmean<=p_bottom);

dp = pmean(foo(2:end)) - pmean(foo(1:end-1));

foo_1 = repmat(dp,[1,size(qq,4),size(qq,1),size(qq,2)]);
foo_2 = permute(foo_1,[3,4,1,2]);

subs = find(isnan(qq));
qq(subs) = 0;
weights(subs)=0;
Qdp = weights.*(qq(:,:,:,:).*double(foo_2));
column = squeeze(sum(Qdp,3))./squeeze(sum(weights.*foo_2,3));



